FROM maven:3-jdk-11-slim

RUN apt-get update && apt-get install sudo

RUN adduser --disabled-password --system --no-create-home --home /usr/src/webgoat --shell /bin/bash app
WORKDIR /usr/src/webgoat

ADD . /usr/src/webgoat
RUN chown app -R /usr/src

USER app
RUN mvn clean install

ADD run.sh /run.sh

CMD '/run.sh'
